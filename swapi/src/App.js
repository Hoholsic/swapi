import React from 'react';
import {Route, withRouter, Switch} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withSuspense} from './hoc/withSuspense';
import './App.css';
import Home from './components/Home/Home';
import NavbarContainer from './components/Navbar/NavbarContainer';
import FirstScreen from './components/Common/FirstScreen/FirstScreen';
import SearchContainer from './components/Search/SearchContainer';
import NotFound from './components/Common/NotFound/NotFound';

const PeopleContainer = React.lazy(() => import('./components/People/PeopleContainer'));
const PeopleInfoContainer = React.lazy(() => import('./components/People/PeopleInfo/PeopleInfoContainer'));
const FilmsContainer = React.lazy(() => import('./components/Films/FilmsContainer'));
const FilmInfoContainer = React.lazy(() => import('./components/Films/FilmInfo/FilmInfoContainer'));
const SpeciesContainer = React.lazy(() => import('./components/Species/SpeciesContainer'));
const SpeciesInfoContainer = React.lazy(() => import('./components/Species/SpeciesInfo/SpeciesInfoContainer'));
const PlanetsContainer = React.lazy(() => import('./components/Planets/PlanetsContainer'));
const PlanetInfoContainer = React.lazy(() => import('./components/Planets/PlanetInfo/PlanetInfoContainer'));
const StarshipsContainer = React.lazy(() => import('./components/Starships/StarshipsContainer'));
const StarshipInfoContainer = React.lazy(() => import('./components/Starships/StarshipInfo/StarshipInfoContainer'));
const VehiclesContainer = React.lazy(() => import('./components/Vehicles/VehiclesContainer'));
const VehicleInfoContainer = React.lazy(() => import('./components/Vehicles/VehicleInfo/VehicleInfoContainer'));

const App = (props) => {
  return (
      <div className="wrapper">
        <NavbarContainer/>
        <Switch>
          <Route exact path='/' render = { () => <FirstScreen/> }/>
          <Route exact path='/home' render = { () => <Home/> }/>
          <Route exact path='/people' render = {withSuspense(PeopleContainer)}/>
          <Route exact path='/people/:id'  render = {withSuspense(PeopleInfoContainer)}/>
          <Route exact path='/films' render = {withSuspense(FilmsContainer)}/>
          <Route exact path='/films/:id' render = {withSuspense(FilmInfoContainer)}/>
          <Route exact path='/species' render = {withSuspense(SpeciesContainer)}/>
          <Route exact path='/species/:id' render = {withSuspense(SpeciesInfoContainer)}/>
          <Route exact path='/planets' render = {withSuspense(PlanetsContainer)}/>
          <Route exact path='/planets/:id' render = {withSuspense(PlanetInfoContainer)}/>
          <Route exact path='/starships' render = {withSuspense(StarshipsContainer)}/>
          <Route exact path='/starships/:id' render = {withSuspense(StarshipInfoContainer)}/>
          <Route exact path='/vehicles' render = {withSuspense(VehiclesContainer)}/>
          <Route exact path='/vehicles/:id' render = {withSuspense(VehicleInfoContainer)}/>
          <Route exact path='/search' render = { () => <SearchContainer/> }/>
          <Route path='*' render = { () => <NotFound/> }/>
        </Switch> 
      </div>
  );
}
export default compose(withRouter, connect())(App);