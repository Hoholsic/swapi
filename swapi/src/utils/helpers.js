const SWAPI_ROOT = "https://swapi.dev/api/";
const SWAPI_PEOPLE = 'people';
const SWAPI_FILMS = 'films';
const SWAPI_SPECIES = 'species';
const SWAPI_PLANETS = 'planets';
const SWAPI_STARSHIPS = 'starships';
const SWAPI_VEHICLES = 'vehicles';

const ING_ROOT_CHARCTERS = 'https://starwars-visualguide.com/assets/img/characters';
const ING_ROOT_FILMS = 'https://starwars-visualguide.com/assets/img/films';
const ING_ROOT_SPECIES = 'https://starwars-visualguide.com/assets/img/species';
const ING_ROOT_PLANETS = 'https://starwars-visualguide.com/assets/img/planets';
const ING_ROOT_STARSHIPS = 'https://starwars-visualguide.com/assets/img/starships';
const ING_ROOT_VEHICLES = 'https://starwars-visualguide.com/assets/img/vehicles';
const IMG_EXTENSION = '.jpg'

const getId = (url, category) => {
	const id = url
		.replace(SWAPI_ROOT+category, '')
		.replace(/\//g, '')
	return id;
}

export const getPeopleId = (url) => getId(url, SWAPI_PEOPLE);
export const getFilmsId = (url) => getId(url, SWAPI_FILMS);
export const getSpeciesId = (url) => getId(url, SWAPI_SPECIES);
export const getPlanetsId = (url) => getId(url, SWAPI_PLANETS);
export const getStarshipsId = (url) => getId(url, SWAPI_STARSHIPS);
export const getVehiclesId = (url) => getId(url, SWAPI_VEHICLES);

export const getPeopleImg = (id) => {return `${ING_ROOT_CHARCTERS}/${id+IMG_EXTENSION}`};
export const getFilmsImg = (id) => {return `${ING_ROOT_FILMS}/${id+IMG_EXTENSION}`};
export const getSpeciesImg = (id) => {return `${ING_ROOT_SPECIES}/${id+IMG_EXTENSION}`};
export const getPlanetsImg = (id) => {return `${ING_ROOT_PLANETS}/${id+IMG_EXTENSION}`};
export const getStarshipsImg = (id) => {return `${ING_ROOT_STARSHIPS}/${id+IMG_EXTENSION}`};
export const getVehiclesImg = (id) => {return `${ING_ROOT_VEHICLES}/${id+IMG_EXTENSION}`};
