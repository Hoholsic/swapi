import {vehiclesAPI} from '../api/api';
import {getVehiclesId, getVehiclesImg, getFilmsId, getFilmsImg, getPeopleId, getPeopleImg} from '../utils/helpers';

const SET_VEHICLES = 'swapi/vehiclesPage/SET_VEHICLES';
const SET_CURRENT_PAGE = 'swapi/vehiclesPage/SET_CURRENT_PAGE';
const SET_VEHICLES_INFO = 'swapi/vehiclesPage/SET_VEHICLES_INFO';
const SET_ESSENCE_TOTAL_COUNT = 'swapi/vehiclesPage/SET_ESSENCE_TOTAL_COUNT';
const SET_VEHICLES_NAME = 'swapi/vehiclesPage/SET_VEHICLES_NAME';
const SET_VEHICLES_PHOTO = 'swapi/vehiclesPage/SET_VEHICLES_PHOTO';
const TOOGLE_IS_FETCHING = 'swapi/vehiclesPage/TOOGLE_IS_FETCHING';
const SET_VEHICLES_FILM = 'swapi/vehiclesPage/SET_VEHICLES_FILM';
const SET_VEHICLES_PEOPLE = 'swapi/vehiclesPage/SET_VEHICLES_PEOPLE';
const ERROR_API = 'swapi/vehiclesPage/ERROR_API';
const ERROR_MESSAGE = 'swapi/vehiclesPage/ERROR_MESSAGE';


let initialState = {
	vehicles: [],
	currentPage: 1,
	totalEssenceCount: 0,
	pageSize: 10,
	vehiclesName: '',
	vehiclesPhoto: ``,
	itemInfo: [],
	films: [],
	people: [],
	isFetching: true,
	isErrorApi: false,
	errorMessage: ''
}

const vehiclesReducer = (state = initialState, action) => {
	switch(action.type) {
		case SET_VEHICLES:
			return {
				...state,
				vehicles: action.vehicles
			};
		case SET_CURRENT_PAGE:
			return {
				...state,
				currentPage: action.currentPage
			};
		case SET_VEHICLES_INFO:
			return {
				...state,
				itemInfo: action.itemInfo
			};
		case SET_VEHICLES_NAME:
			return {
				...state,
				vehiclesName: action.vehiclesName
			};
		case SET_VEHICLES_PHOTO:
			return {
				...state,
				vehiclesPhoto: action.vehiclesPhoto
			};
		case SET_VEHICLES_FILM: 
			return {
				...state,
				films: action.films
			};
		case SET_VEHICLES_PEOPLE: 
			return {
				...state,
				people: action.people
			};
		case SET_ESSENCE_TOTAL_COUNT: 
			return {
				...state,
				totalEssenceCount: action.totalEssenceCount
			};
		case TOOGLE_IS_FETCHING:
			return {
				...state,
				isFetching: action.isFetching
			};
		case ERROR_API:
			return {
				...state,
				isErrorApi: action.isErrorApi
			};
		case ERROR_MESSAGE:
			return {
				...state,
				errorMessage: action.errorMessage
			}
		default:
			return state;
	}
}

export const setVehicles = (vehicles) => ({type: SET_VEHICLES, vehicles});
export const setCurrentPage = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setVehiclesInfo = (itemInfo) => ({type: SET_VEHICLES_INFO, itemInfo});
export const setTotalEssenceCount = (totalEssenceCount) => ({type: SET_ESSENCE_TOTAL_COUNT, totalEssenceCount});
export const setVehiclesName = (vehiclesName) => ({type: SET_VEHICLES_NAME, vehiclesName});
export const setVehiclesPhoto = (vehiclesPhoto) => ({type: SET_VEHICLES_PHOTO, vehiclesPhoto});
export const toogleIsFetching = (isFetching) => ({type: TOOGLE_IS_FETCHING, isFetching});
export const setVehiclesFilms = (films) => ({type: SET_VEHICLES_FILM, films});
export const setVehiclesPeople = (people) => ({type: SET_VEHICLES_PEOPLE, people});
export const setErrorApi = (isErrorApi) => ({type: ERROR_API, isErrorApi});
export const setErrorMessage = (errorMessage) => ({type: ERROR_MESSAGE, errorMessage});

export const requestVehicles = (currentPage) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		dispatch(setCurrentPage(currentPage));
		let data = await vehiclesAPI.getVehicles(currentPage);
		let vehiclesList = data.results.map(({name, url}) => {
			const id = getVehiclesId(url);
			const img = getVehiclesImg(id);
			return{name, id, img}
		});
		dispatch(toogleIsFetching(false));
		dispatch(setVehicles(vehiclesList));
		dispatch(setTotalEssenceCount(data.count)); 
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export const requestVehiclesInfo = (id) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		let data = await vehiclesAPI.getInfo(id);
		dispatch(setVehiclesInfo([
				{title: 'Model', value: data.model},
				{title: 'Manufacturer', value: data.manufacturer},
				{title: 'Class', value: data.vehicle_class},
				{title: 'Cost', value: data.cost_in_credits},
				{title: 'Speed', value: data.max_atmosphering_speed},
				{title: 'Length', value: data.length},
				{title: 'Crew', value: data.crew},
				{title: 'Passengers', value: data.passengers},
				{title: 'Cargo Capacity', value: data.cargo_capacity},
				{title: 'Consumables', value: data.consumables}
			]));  
		dispatch(setVehiclesName(data.name));
		let filmsList = data.films.map( url => {
			const id = getFilmsId(url);
			const img = getFilmsImg(id);
			return {id, img}
		});
		dispatch(setVehiclesFilms(filmsList));
		let peopleList = data.pilots.map( url => {
			const id = getPeopleId(url);
			const img = getPeopleImg(id);
			return {id, img}
		});
		dispatch(setVehiclesPeople(peopleList));
		dispatch(setVehiclesPhoto(getVehiclesImg(id))); 
		dispatch(toogleIsFetching(false));
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export default vehiclesReducer;