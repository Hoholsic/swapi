import {speciesAPI} from '../api/api';
import {getSpeciesId, getSpeciesImg, getFilmsId, getFilmsImg, getPeopleId, getPeopleImg} from '../utils/helpers';

const SET_SPECIES = 'swapi/speciesPage/SET_SPECIES';
const SET_CURRENT_PAGE = 'swapi/speciesPage/SET_CURRENT_PAGE';
const SET_SPECIES_INFO = 'swapi/speciesPage/SET_SPECIES_INFO';
const SET_ESSENCE_TOTAL_COUNT = 'swapi/speciesPage/SET_ESSENCE_TOTAL_COUNT';
const SET_SPECIES_NAME = 'swapi/speciesPage/SET_SPECIES_NAME';
const SET_SPECIES_PHOTO = 'swapi/speciesPage/SET_SPECIES_PHOTO';
const TOOGLE_IS_FETCHING = 'swapi/speciesPage/TOOGLE_IS_FETCHING';
const SET_SPECIES_FILM = 'swapi/speciesPage/SET_SPECIES_FILM';
const SET_SPECIES_PEOPLE = 'swapi/speciesPage/SET_SPECIES_PEOPLE';
const ERROR_API = 'swapi/speciesPage/ERROR_API';
const ERROR_MESSAGE = 'swapi/speciesPage/ERROR_MESSAGE';

let initialState = {
	species: [],
	currentPage: 1,
	totalEssenceCount: 0,
	pageSize: 10,
	speciesName: '',
	speciesPhoto: ``,
	itemInfo: [],
	films: [],
	people: [],
	isFetching: true,
	isErrorApi: false,
	errorMessage: ''
}

const speciesReducer = (state = initialState, action) => {
	switch(action.type) {
		case SET_SPECIES:
			return {
				...state,
				species: action.species
			};
		case SET_CURRENT_PAGE:
			return {
				...state,
				currentPage: action.currentPage
			};
		case SET_SPECIES_INFO:
			return {
				...state,
				itemInfo: action.itemInfo
			};
		case SET_SPECIES_NAME:
			return {
				...state,
				speciesName: action.speciesName
			};
		case SET_SPECIES_PHOTO:
			return {
				...state,
				speciesPhoto: action.speciesPhoto
			};
		case SET_SPECIES_FILM: 
			return {
				...state,
				films: action.films
			};
		case SET_SPECIES_PEOPLE: 
			return {
				...state,
				people: action.people
			};
		case SET_ESSENCE_TOTAL_COUNT: 
			return {
				...state,
				totalEssenceCount: action.totalEssenceCount
			};
		case TOOGLE_IS_FETCHING:
			return {
				...state,
				isFetching: action.isFetching
			};
		case ERROR_API:
			return {
				...state,
				isErrorApi: action.isErrorApi
			};
		case ERROR_MESSAGE:
			return {
				...state,
				errorMessage: action.errorMessage
			};
		default:
			return state;
	}
}

export const setSpecies = (species) => ({type: SET_SPECIES, species});
export const setCurrentPage = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setSpeciesInfo = (itemInfo) => ({type: SET_SPECIES_INFO, itemInfo});
export const setTotalEssenceCount = (totalEssenceCount) => ({type: SET_ESSENCE_TOTAL_COUNT, totalEssenceCount});
export const setSpeciesName = (speciesName) => ({type: SET_SPECIES_NAME, speciesName});
export const setSpeciesPhoto = (speciesPhoto) => ({type: SET_SPECIES_PHOTO, speciesPhoto});
export const toogleIsFetching = (isFetching) => ({type: TOOGLE_IS_FETCHING, isFetching});
export const setSpeciesFilms = (films) => ({type: SET_SPECIES_FILM, films});
export const setSpeciesPeople = (people) => ({type: SET_SPECIES_PEOPLE, people});
export const setErrorApi = (isErrorApi) => ({type: ERROR_API, isErrorApi});
export const setErrorMessage = (errorMessage) => ({type: ERROR_MESSAGE, errorMessage});

export const requestSpecies = (currentPage) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		dispatch(setCurrentPage(currentPage));
		let data = await speciesAPI.getSpecies(currentPage);
		let speciesList = data.results.map(({name, url}) => {
			const id = getSpeciesId(url);
			const img = getSpeciesImg(id);
			return{name, id, img}
		});
		dispatch(toogleIsFetching(false));
		dispatch(setSpecies(speciesList));
		dispatch(setTotalEssenceCount(data.count)); 
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export const requestSpeciesInfo = (id) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		let data = await speciesAPI.getInfo(id);
		dispatch(setSpeciesInfo([
				{title: 'Classification', value: data.classification},
				{title: 'Designation', value: data.designation},
				{title: 'Language', value: data.language},
				{title: 'Average lifespan', value: data.average_lifespan},
				{title: 'Average height', value: data.average_height},
				{title: 'Skin color', value: data.skin_colors},
				{title: 'Eye color', value: data.eye_colors},
				{title: 'Hair color', value: data.hair_colors}
			]));  
		dispatch(setSpeciesName(data.name));
		let filmsList = data.films.map( url => {
			const id = getFilmsId(url);
			const img = getFilmsImg(id);
			return {id, img}
		});
		dispatch(setSpeciesFilms(filmsList));
		let peopleList = data.people.map( url => {
			const id = getPeopleId(url);
			const img = getPeopleImg(id);
			return {id, img}
		});
		dispatch(setSpeciesPeople(peopleList));
		dispatch(setSpeciesPhoto(getSpeciesImg(id))); 
		dispatch(toogleIsFetching(false));
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export default speciesReducer;