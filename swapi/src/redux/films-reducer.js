import {filmsAPI, planetsAPI, starshipsAPI, vehiclesAPI} from '../api/api';
import {getFilmsId, getFilmsImg, getPeopleId, getPeopleImg, 
	getSpeciesId, getSpeciesImg, getPlanetsId, getStarshipsId, getVehiclesId} from '../utils/helpers';

const SET_FILMS = 'swapi/filmsPage/SET_FILMS';
const SET_CURRENT_PAGE = 'swapi/filmsPage/SET_CURRENT_PAGE';
const SET_FILM_INFO = 'swapi/filmsPage/SET_FILM_INFO';
const SET_FILM_TITLE = 'swapi/filmsPage/SET_FILM_TITLE';
const SET_FILM_POSTER = 'swapi/filmsPage/SET_FILM_POSTER';
const SET_FILM_CHARACTERS = 'swapi/filmsPage/SET_FILM_CHARACTERS';
const SET_FILM_SPECIES = 'swapi/filmsPage/SET_FILM_SPECIES';
const SET_FILM_PLANETS = 'swapi/filmsPage/SET_FILM_PLANETS';
const SET_FILM_STARSHIPS = 'swapi/filmsPage/SET_FILM_STARSHIPS';
const SET_FILM_VEHICLES = 'swapi/filmsPage/SET_FILM_VEHICLES';
const TOOGLE_IS_FETCHING = 'swapi/filmsPage/TOOGLE_IS_FETCHING';
const ERROR_API = 'swapi/filmsPage/ERROR_API';
const ERROR_MESSAGE = 'swapi/filmsPage/ERROR_MESSAGE';

let initialState = {
	films: [],
	currentPage: 1,
	itemInfo: [],
	filmTitle: '',
	filmPoster: ``,
	people: [],
	species: [],
	planets: [],
	starships: [],
	vehicles: [],
	isFetching: true,
	isErrorApi: false,
	errorMessage: ''
}

const filmsReducer = (state = initialState, action) => {
	switch(action.type) {
		case SET_FILMS:
			return {
				...state,
				films: action.films
			};
		case SET_CURRENT_PAGE:
			return {
				...state,
				currentPage: action.currentPage
			};
		case SET_FILM_INFO:
			return {
				...state,
				itemInfo: action.itemInfo
			};
		case SET_FILM_TITLE:
			return {
				...state,
				filmTitle: action.filmTitle
			};
		case SET_FILM_POSTER:
			return {
				...state,
				filmPoster: action.filmPoster
			};
		case SET_FILM_CHARACTERS:
			return {
				...state,
				people: action.people
			};
		case SET_FILM_SPECIES:
			return {
				...state,
				species: action.species
			};
		case SET_FILM_PLANETS:
			return {
				...state,
				planets: action.planets
			};
		case SET_FILM_STARSHIPS:
			return {
				...state,
				starships: action.starships
			};
		case SET_FILM_VEHICLES:
			return {
				...state,
				vehicles: action.vehicles
			};
		case TOOGLE_IS_FETCHING:
			return {
				...state,
				isFetching: action.isFetching
			};
		case ERROR_API:
			return {
				...state,
				isErrorApi: action.isErrorApi
			};
		case ERROR_MESSAGE:
			return {
				...state,
				errorMessage: action.errorMessage
			};
		default:
			return state;
	}
}

export const setFilms = (films) => ({type: SET_FILMS, films});
export const setCurrentPage = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setFilmInfo = (itemInfo) => ({type: SET_FILM_INFO, itemInfo});
export const setFilmTitle = (filmTitle) => ({type: SET_FILM_TITLE, filmTitle});
export const setFilmPoster = (filmPoster) => ({type: SET_FILM_POSTER, filmPoster});
export const setFilmCharacters = (people) => ({type: SET_FILM_CHARACTERS, people});
export const setFilmSpecies = (species) => ({type: SET_FILM_SPECIES, species});
export const setFilmPlanets = (planets) => ({type: SET_FILM_PLANETS, planets});
export const setFilmStarships = (starships) => ({type: SET_FILM_STARSHIPS, starships});
export const setFilmVehicles = (vehicles) => ({type: SET_FILM_VEHICLES, vehicles});
export const toogleIsFetching = (isFetching) => ({type: TOOGLE_IS_FETCHING, isFetching});
export const setErrorApi = (isErrorApi) => ({type: ERROR_API, isErrorApi});
export const setErrorMessage = (errorMessage) => ({type: ERROR_MESSAGE, errorMessage});

export const requestFilms = (currentPage) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		dispatch(setCurrentPage(currentPage));
		let data = await filmsAPI.getFilms(currentPage);
		let filmsList = data.results.map(({title, episode_id, url}) => {
			const id = getFilmsId(url);
			const img = getFilmsImg(id);
			return{title, episode_id, id, img}
		});
		dispatch(toogleIsFetching(false));
		dispatch(setFilms(filmsList));
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export const requestFilmInfo = (id) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		let data = await filmsAPI.getInfo(id);
		dispatch(setFilmInfo([
				{title: 'Release date', value: data.release_date},
				{title: 'Director', value: data.director},
				{title: 'Episode', value: data.episode_id},
				{title: 'Producer(s)', value: data.producer},
				{title: 'Opening crawl', value: data.opening_crawl}
			]));  
		dispatch(setFilmTitle(data.title));
		let charactersList = data.characters.map( url => {
			const id = getPeopleId(url);
			const img = getPeopleImg(id);
			return {id, img}
		});
		dispatch(setFilmCharacters(charactersList));
		let speciesList = data.species.map( url => {
			const id = getSpeciesId(url);
			const img = getSpeciesImg(id);
			return {id, img}
		});
		dispatch(setFilmSpecies(speciesList));
		dispatch(requestInfo(data.planets, getPlanetsId, planetsAPI, setFilmPlanets));
		dispatch(requestInfo(data.starships, getStarshipsId, starshipsAPI, setFilmStarships));
		dispatch(requestInfo(data.vehicles, getVehiclesId, vehiclesAPI, setFilmVehicles));
		dispatch(setFilmPoster(getFilmsImg(id))); 
		dispatch(toogleIsFetching(false));
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};
const requestInfo = (urls, funcId, objectAPI, action) => async (dispatch) => {
	let res = await Promise.all(urls.map(async (item): Promise<number> => {
		const id = funcId(item);
		return await objectAPI.getInfo(id);    
	}));
	let itemList = res.map(({name, url}) => {
		const id = funcId(url);
		return {id, name}
	});
	dispatch(action(itemList));
}

export default filmsReducer;