import {peopleAPI, speciesAPI, planetsAPI, starshipsAPI, vehiclesAPI} from '../api/api';
import {getPeopleId, getPeopleImg, getFilmsId, getFilmsImg, 
		getSpeciesId, getPlanetsId, getStarshipsId, getVehiclesId} from '../utils/helpers';

const SET_PEOPLE = 'swapi/peoplePage/SET_PEOPLE';
const SET_CURRENT_PAGE = 'swapi/peoplePage/SET_CURRENT_PAGE';
const SET_ESSENCE_TOTAL_COUNT = 'swapi/peoplePage/SET_ESSENCE_TOTAL_COUNT';
const SET_PERSON = 'swapi/peoplePage/SET_PERSON';
const SET_PERSON_NAME = 'swapi/peoplePage/SET_PERSON_NAME';
const SET_PERSON_PHOTO = 'swapi/peoplePage/SET_PERSON_PHOTO';
const SET_PERSON_FILMS = 'swapi/peoplePage/SET_PERSON_FILMS';
const SET_PERSON_SPECIES = 'swapi/peoplePage/SET_PERSON_SPECIES';
const SET_PERSON_HOMEWORLD = 'swapi/peoplePage/SET_PERSON_HOMEWORLD';
const SET_PERSON_STARSHIPS = 'swapi/peoplePage/SET_PERSON_STARSHIPS';
const SET_PERSON_VEHICLES = 'swapi/peoplePage/SET_PERSON_VEHICLES';
const TOOGLE_IS_FETCHING = 'swapi/peoplePage/TOOGLE_IS_FETCHING';
const ERROR_API = 'swapi/peoplePage/ERROR_API';
const ERROR_MESSAGE = 'swapi/peoplePage/ERROR_MESSAGE';

let initialState = {
	people: [],
	pageSize: 10,
	totalEssenceCount: 0,
	currentPage: 1,
	itemInfo: [],
	personName: '',
	personPhoto: ``,
	films: [],
	personSpecies: [],
	homeworld: [],
	starships: [],
	vehicles: [],
	isFetching: true,
	isErrorApi: false,
	errorMessage: ''
}

const peopleReducer = (state = initialState, action) => {
	switch(action.type) {
		case SET_PEOPLE:
			return {
				...state,
				people: action.people
			};
		case SET_CURRENT_PAGE:
			return {
				...state,
				currentPage: action.currentPage
			};
		case SET_ESSENCE_TOTAL_COUNT: 
			return {
				...state,
				totalEssenceCount: action.totalEssenceCount
			};
		case SET_PERSON:
			return {
				...state,
				itemInfo: action.itemInfo
			};
		case SET_PERSON_NAME:
			return {
				...state,
				personName: action.personName
			};
		case SET_PERSON_PHOTO:
			return {
				...state,
				personPhoto: action.personPhoto
			};
		case SET_PERSON_FILMS:
			return {
				...state,
				films: action.films
			};
		case SET_PERSON_SPECIES:
			return {
				...state,
				personSpecies: action.personSpecies
			};
		case SET_PERSON_HOMEWORLD:
			return {
				...state,
				homeworld: action.homeworld
			};
		case SET_PERSON_STARSHIPS:
			return {
				...state,
				starships: action.starships
			};
		case SET_PERSON_VEHICLES:
			return {
				...state,
				vehicles: action.vehicles
			};
		case TOOGLE_IS_FETCHING:
			return {
				...state,
				isFetching: action.isFetching
			};
		case ERROR_API:
			return {
				...state,
				isErrorApi: action.isErrorApi
			};
		case ERROR_MESSAGE:
			return {
				...state,
				errorMessage: action.errorMessage
			};
		default:
			return state;
	}
}

export const setPeople = (people) => ({type: SET_PEOPLE, people});
export const setPerson = (itemInfo) => ({type: SET_PERSON, itemInfo});
export const setCurrentPage = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setTotalEssenceCount = (totalEssenceCount) => ({type: SET_ESSENCE_TOTAL_COUNT, totalEssenceCount});
export const setPersonName = (personName) => ({type: SET_PERSON_NAME, personName});
export const setPersonPhoto = (personPhoto) => ({type: SET_PERSON_PHOTO, personPhoto});
export const setPersonFilms = (films) => ({type: SET_PERSON_FILMS, films});
export const setPersonSpecies = (personSpecies) => ({type: SET_PERSON_SPECIES, personSpecies});
export const setPersonHomeworld = (homeworld) => ({type: SET_PERSON_HOMEWORLD, homeworld});
export const setPersonStarships = (starships) => ({type: SET_PERSON_STARSHIPS, starships});
export const setPersonVehicles = (vehicles) => ({type: SET_PERSON_VEHICLES, vehicles});
export const toogleIsFetching = (isFetching) => ({type: TOOGLE_IS_FETCHING, isFetching});
export const setErrorApi = (isErrorApi) => ({type: ERROR_API, isErrorApi});
export const setErrorMessage = (errorMessage) => ({type: ERROR_MESSAGE, errorMessage});

export const requestPeople = (currentPage) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		dispatch(setCurrentPage(currentPage));
		let responsee = await peopleAPI.getPeople(currentPage);
		let peopleList = responsee.data.results.map(({name, url}) => {
			const id = getPeopleId(url);
			const img = getPeopleImg(id);
			return{name, id, img}
		});
		dispatch(toogleIsFetching(false));
		dispatch(setPeople(peopleList));
		dispatch(setTotalEssenceCount(responsee.data.count)); 
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export const requestPerson = (id) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		let data = await peopleAPI.getInfo(id);
		dispatch(setPerson([
				{title: 'Birth year', value: data.birth_year},
				{title: 'Gender', value: data.gender},
				{title: 'Height', value: data.height},
				{title: 'Mass', value: data.mass},
				{title: 'Skin color', value: data.skin_color},
				{title: 'Eye color', value: data.eye_color},
				{title: 'Hair color', value: data.hair_color}
			]));  
		dispatch(setPersonName(data.name));
		let filmsList = data.films.map( url => {
			const id = getFilmsId(url);
			const img = getFilmsImg(id);
			return {id, img}
		});
		dispatch(setPersonFilms(filmsList));
		dispatch(requestPersonHomeworld(data.homeworld));
		dispatch(requestInfo(data.species, getSpeciesId, speciesAPI, setPersonSpecies));
		dispatch(requestInfo(data.starships, getStarshipsId, starshipsAPI, setPersonStarships));
		dispatch(requestInfo(data.vehicles, getVehiclesId, vehiclesAPI, setPersonVehicles));
		dispatch(setPersonPhoto(getPeopleImg(id))); 
		dispatch(toogleIsFetching(false));	
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};
const requestPersonHomeworld = (url) => async (dispatch) => {
	const id = getPlanetsId(url);
	let data = await planetsAPI.getInfo(id);   
	dispatch(setPersonHomeworld([
			{title: 'homeworld', value: data.name, id}
		]));
};
const requestInfo = (urls, funcId, objectAPI, action) => async (dispatch) => {
	let res = await Promise.all(urls.map(async (item): Promise<number> => {
		const id = funcId(item);
		return await objectAPI.getInfo(id);    
	}));
	let itemList = res.map(({name, url}) => {
		const id = funcId(url);
		return {id, name}
	});
	dispatch(action(itemList));
}

export default peopleReducer;