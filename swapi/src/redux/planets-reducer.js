import {planetsAPI} from '../api/api';
import {getPlanetsId, getPlanetsImg, getPeopleId, getPeopleImg, getFilmsId, getFilmsImg} from '../utils/helpers';

const SET_PLANETS = 'swapi/planetsPage/SET_PLANETS';
const SET_CURRENT_PAGE = 'swapi/planetsPage/SET_CURRENT_PAGE';
const SET_ESSENCE_TOTAL_COUNT = 'swapi/planetsPage/SET_ESSENCE_TOTAL_COUNT';
const SET_PLENET_INFO = 'swapi/planetsPage/SET_PLENET_INFO';
const SET_PLENET_NAME = 'swapi/planetsPage/SET_PERSON_NAME';
const SET_PLENET_PHOTO = 'swapi/planetsPage/SET_PERSON_PHOTO';
const SET_PLENET_FILMS = 'swapi/planetsPage/SET_PLENET_FILMS';
const SET_PLENET_PEOPLE = 'swapi/planetsPage/SET_PLENET_PEOPLE';
const TOOGLE_IS_FETCHING = 'swapi/planetsPage/TOOGLE_IS_FETCHING';
const ERROR_API = 'swapi/planetsPage/ERROR_API';
const ERROR_MESSAGE = 'swapi/planetsPage/ERROR_MESSAGE';

let initialState = {
	planets: [],
	pageSize: 10,
	totalEssenceCount: 0,
	currentPage: 1,
	itemInfo: [],
	plenetName: '',
	plenetPhoto: ``,
	films: [],
	people: [],
	isFetching: true,
	isErrorApi: false,
	errorMessage: ''
}

const planetsReducer = (state = initialState, action) => {
	switch(action.type) {
		case SET_PLANETS:
			return {
				...state,
				planets: action.planets
			};
		case SET_CURRENT_PAGE:
			return {
				...state,
				currentPage: action.currentPage
			};
		case SET_ESSENCE_TOTAL_COUNT: 
			return {
				...state,
				totalEssenceCount: action.totalEssenceCount
			};
		case SET_PLENET_INFO:
			return {
				...state,
				itemInfo: action.itemInfo
			};
		case SET_PLENET_NAME:
			return {
				...state,
				plenetName: action.plenetName
			};
		case SET_PLENET_PHOTO:
			return {
				...state,
				plenetPhoto: action.plenetPhoto
			};
		case SET_PLENET_FILMS:
			return {
				...state,
				films: action.films
			};
		case SET_PLENET_PEOPLE:
			return {
				...state,
				people: action.people
			};
		case TOOGLE_IS_FETCHING:
			return {
				...state,
				isFetching: action.isFetching
			};
		case ERROR_API:
			return {
				...state,
				isErrorApi: action.isErrorApi
			};
		case ERROR_MESSAGE:
			return {
				...state,
				errorMessage: action.errorMessage
			};
		default:
			return state;
	}
}

export const setPlanets = (planets) => ({type: SET_PLANETS, planets});
export const setPlanetInfo = (itemInfo) => ({type: SET_PLENET_INFO, itemInfo});
export const setCurrentPage = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setTotalEssenceCount = (totalEssenceCount) => ({type: SET_ESSENCE_TOTAL_COUNT, totalEssenceCount});
export const setPlenetName = (plenetName) => ({type: SET_PLENET_NAME, plenetName});
export const setPlenetPhoto = (plenetPhoto) => ({type: SET_PLENET_PHOTO, plenetPhoto});
export const setPlenetFilms = (films) => ({type: SET_PLENET_FILMS, films});
export const setPlenetPeople = (people) => ({type: SET_PLENET_PEOPLE, people});
export const toogleIsFetching = (isFetching) => ({type: TOOGLE_IS_FETCHING, isFetching});
export const setErrorApi = (isErrorApi) => ({type: ERROR_API, isErrorApi});
export const setErrorMessage = (errorMessage) => ({type: ERROR_MESSAGE, errorMessage});

export const requestPlanets = (currentPage) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		dispatch(setCurrentPage(currentPage));
		let data = await planetsAPI.getPlanets(currentPage);
		let planetsList = data.results.map(({name, url}) => {
			const id = getPlanetsId(url);
			const img = getPlanetsImg(id);
			return{id, name, img}		
		});
		dispatch(toogleIsFetching(false));
		dispatch(setPlanets(planetsList));	
		dispatch(setTotalEssenceCount(data.count));
		dispatch(setErrorApi(false)); 
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export const requestPlanetInfo = (id) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		let data = await planetsAPI.getInfo(id);
		dispatch(setPlanetInfo([
				{title: 'Population', value: data.population},
				{title: 'Diameter', value: data.diameter},
				{title: 'Orbital period', value: data.orbital_period},
				{title: 'Rotation period', value: data.rotation_period},
				{title: 'Gravity', value: data.gravity},
				{title: 'Climate', value: data.climate},
				{title: 'Surface water', value: data.surface_water},
				{title: 'Terrain', value: data.terrain}
			]));  
		dispatch(setPlenetName(data.name));
		dispatch(setPlenetPhoto(getPlanetsImg(id))); 
		let filmsList = data.films.map( url => {
			const id = getFilmsId(url);
			const img = getFilmsImg(id);
			return {id, img}
		});
		dispatch(setPlenetFilms(filmsList));
		let peopleList = data.residents.map( url => {
			const id = getPeopleId(url);
			const img = getPeopleImg(id);
			return {id, img}
		});
		dispatch(setPlenetPeople(peopleList));
		dispatch(toogleIsFetching(false));
		dispatch(setErrorApi(false));
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export default planetsReducer;