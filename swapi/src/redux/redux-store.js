import {combineReducers, createStore, applyMiddleware} from "redux";
import thunkMiddleware from "redux-thunk";
import peopleReducer from "./people-reducer";
import searchReducer from "./search-reducer";
import filmsReducer from "./films-reducer";
import speciesReducer from "./species-reducer";
import planetsReducer from "./planets-reducer";
import starshipsReducer from "./starships-reducer";
import vehiclesReducer from "./vehicles-reducer";

let reducers = combineReducers({
		peoplePage: peopleReducer,
		searchPage: searchReducer,
		filmsPage: filmsReducer,
		speciesPage: speciesReducer,
		planetsPage: planetsReducer,
		starshipsPage: starshipsReducer,
		vehiclesPage: vehiclesReducer
	});

let store = createStore(reducers, applyMiddleware(thunkMiddleware));

export default store;