import {starshipsAPI} from '../api/api';
import {getStarshipsId, getStarshipsImg, getFilmsId, getFilmsImg, getPeopleId, getPeopleImg} from '../utils/helpers';

const SET_STARSHIPS = 'swapi/starshipsPage/SET_STARSHIPS';
const SET_CURRENT_PAGE = 'swapi/starshipsPage/SET_CURRENT_PAGE';
const SET_STARSHIPS_INFO = 'swapi/starshipsPage/SET_STARSHIPS_INFO';
const SET_ESSENCE_TOTAL_COUNT = 'swapi/starshipsPage/SET_ESSENCE_TOTAL_COUNT';
const SET_STARSHIPS_NAME = 'swapi/starshipsPage/SET_STARSHIPS_NAME';
const SET_STARSHIPS_PHOTO = 'swapi/starshipsPage/SET_STARSHIPS_PHOTO';
const TOOGLE_IS_FETCHING = 'swapi/starshipsPage/TOOGLE_IS_FETCHING';
const SET_STARSHIPS_FILM = 'swapi/starshipsPage/SET_STARSHIPS_FILM';
const SET_STARSHIPS_PEOPLE = 'swapi/starshipsPage/SET_STARSHIPS_PEOPLE';
const ERROR_API = 'swapi/starshipsPage/ERROR_API';
const ERROR_MESSAGE = 'swapi/starshipsPage/ERROR_MESSAGE';

let initialState = {
	starships: [],
	currentPage: 1,
	totalEssenceCount: 0,
	pageSize: 10,
	starshipsName: '',
	starshipsPhoto: ``,
	itemInfo: [],
	films: [],
	people: [],
	isFetching: true,
	isErrorApi: false,
	errorMessage: ''
}

const starshipsReducer = (state = initialState, action) => {
	switch(action.type) {
		case SET_STARSHIPS:
			return {
				...state,
				starships: action.starships
			};
		case SET_CURRENT_PAGE:
			return {
				...state,
				currentPage: action.currentPage
			};
		case SET_STARSHIPS_INFO:
			return {
				...state,
				itemInfo: action.itemInfo
			};
		case SET_STARSHIPS_NAME:
			return {
				...state,
				starshipsName: action.starshipsName
			};
		case SET_STARSHIPS_PHOTO:
			return {
				...state,
				starshipsPhoto: action.starshipsPhoto
			};
		case SET_STARSHIPS_FILM: 
			return {
				...state,
				films: action.films
			};
		case SET_STARSHIPS_PEOPLE: 
			return {
				...state,
				people: action.people
			};
		case SET_ESSENCE_TOTAL_COUNT: 
			return {
				...state,
				totalEssenceCount: action.totalEssenceCount
			};
		case TOOGLE_IS_FETCHING:
			return {
				...state,
				isFetching: action.isFetching
			};
		case ERROR_API:
			return {
				...state,
				isErrorApi: action.isErrorApi
			};
		case ERROR_MESSAGE:
			return {
				...state,
				errorMessage: action.errorMessage
			};
		default:
			return state;
	}
}

export const setStarships = (starships) => ({type: SET_STARSHIPS, starships});
export const setCurrentPage = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setStarshipsInfo = (itemInfo) => ({type: SET_STARSHIPS_INFO, itemInfo});
export const setTotalEssenceCount = (totalEssenceCount) => ({type: SET_ESSENCE_TOTAL_COUNT, totalEssenceCount});
export const setStarshipsName = (starshipsName) => ({type: SET_STARSHIPS_NAME, starshipsName});
export const setStarshipsPhoto = (starshipsPhoto) => ({type: SET_STARSHIPS_PHOTO, starshipsPhoto});
export const toogleIsFetching = (isFetching) => ({type: TOOGLE_IS_FETCHING, isFetching});
export const setStarshipsFilms = (films) => ({type: SET_STARSHIPS_FILM, films});
export const setStarshipsPeople = (people) => ({type: SET_STARSHIPS_PEOPLE, people});
export const setErrorApi = (isErrorApi) => ({type: ERROR_API, isErrorApi});
export const setErrorMessage = (errorMessage) => ({type: ERROR_MESSAGE, errorMessage});

export const requestStarships = (currentPage) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		dispatch(setCurrentPage(currentPage));
		let data = await starshipsAPI.getStarships(currentPage);
		let starshipsList = data.results.map(({name, url}) => {
			const id = getStarshipsId(url);
			const img = getStarshipsImg(id);
			return{name, id, img}
		});
		dispatch(toogleIsFetching(false));
		dispatch(setStarships(starshipsList));
		dispatch(setTotalEssenceCount(data.count)); 
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export const requestStarshipsInfo = (id) => async (dispatch) => {
	try{
		dispatch(toogleIsFetching(true));
		let data = await starshipsAPI.getInfo(id);
		dispatch(setStarshipsInfo([
				{title: 'Model', value: data.model},
				{title: 'Manufacturer', value: data.manufacturer},
				{title: 'Class', value: data.starship_class},
				{title: 'Cost', value: data.cost_in_credits},
				{title: 'Speed', value: data.max_atmosphering_speed},
				{title: 'Hyperdrive Rating', value: data.hyperdrive_rating},
				{title: 'MGLT', value: data.MGLT},
				{title: 'Length', value: data.length},
				{title: 'Crew', value: data.crew},
				{title: 'Passengers', value: data.passengers},
				{title: 'Cargo Capacity', value: data.cargo_capacity},
				{title: 'Consumables', value: data.consumables}
			]));  
		dispatch(setStarshipsName(data.name));
		let filmsList = data.films.map( url => {
			const id = getFilmsId(url);
			const img = getFilmsImg(id);
			return {id, img}
		});
		dispatch(setStarshipsFilms(filmsList));
		let peopleList = data.pilots.map( url => {
			const id = getPeopleId(url);
			const img = getPeopleImg(id);
			return {id, img}
		});
		dispatch(setStarshipsPeople(peopleList));
		dispatch(setStarshipsPhoto(getStarshipsImg(id))); 
		dispatch(toogleIsFetching(false));
		dispatch(setErrorApi(false));		
	}catch(error){
		dispatch(setErrorApi(true));
		dispatch(setErrorMessage(error.message));
	}
};

export default starshipsReducer;




