import {searchAPI} from '../api/api';
import {getPeopleId, getPeopleImg} from '../utils/helpers';

const SET_CHARACTERS = 'swapi/searchPage/SET_CHARACTERS';
const SET_VALUE_INPUT = 'swapi/searchPage/SET_VALUE_INPUT';
const TOOGLE_IS_FETCHING = 'swapi/peoplePage/TOOGLE_IS_FETCHING';

let initialState = {
	characters: [],
	valueInput: '',
	isFetching: true,
}
	
const searchReducer = (state = initialState, action) => {
	switch(action.type) {
		case SET_CHARACTERS:
			return {
				...state,
				characters: action.characters
			};
		case SET_VALUE_INPUT:
			return {
				...state,
				valueInput: action.valueInput
			};
		case TOOGLE_IS_FETCHING:
			return {
				...state,
				isFetching: action.isFetching
			};
		default:
			return state;
	}
}

export const setCharacters = (characters) => ({type: SET_CHARACTERS, characters});
export const setValueInput = (valueInput) => ({type: SET_VALUE_INPUT, valueInput});
export const toogleIsFetching = (isFetching) => ({type: TOOGLE_IS_FETCHING, isFetching});

export const searchCharacter = (valueInput) => async (dispatch) => {
	dispatch(toogleIsFetching(true));
	dispatch(setValueInput(valueInput));
	let data = await searchAPI.getCharacters(valueInput);
	let peopleList = data.results.map(({name, url}) => {
		const id = getPeopleId(url);
		const img = getPeopleImg(id);
		return{name, id, img, url}
	});
	dispatch(toogleIsFetching(false));
	dispatch(setCharacters(peopleList));
};

export default searchReducer;