import * as axios from 'axios';

const instance = axios.create({
	baseURL: 'https://swapi.dev/api/'
})

export const peopleAPI = {
	getPeople (currentPage) {
		return instance.get(`people/?page=${currentPage}`).then(response => {
			return response});
	},
	getInfo (id) {
		return instance.get(`people/${id}/`).then(response => {
			return response.data});
	}
}
export const searchAPI = {
	getCharacters (valueInput) {
		return instance.get(`people/?search=${valueInput}`).then(response => {
			return response.data});
	},
}
export const filmsAPI = {
	getFilms (currentPage) {
		return instance.get(`films/?page=${currentPage}`).then(response => {
			return response.data});
	},
	getInfo (id) {
		return instance.get(`films/${id}/`).then(response => {
			return response.data});
	}
}
export const speciesAPI = {
	getSpecies (currentPage) {
		return instance.get(`species/?page=${currentPage}`).then(response => {
			return response.data});
	},
	getInfo (id) {
		return instance.get(`species/${id}/`).then(response => {
			return response.data});
	}
}
export const planetsAPI = {
	getPlanets (currentPage) {
		return instance.get(`planets/?page=${currentPage}`).then(response => {
			return response.data});
	},
	getInfo (id) {
		return instance.get(`planets/${id}/`).then(response => {
			return response.data});
	}
}
export const starshipsAPI = {
	getStarships (currentPage) {
		return instance.get(`starships/?page=${currentPage}`).then(response => {
			return response.data});
	},
	getInfo (id) {
		return instance.get(`starships/${id}/`).then(response => {
			return response.data});
	}
}
export const vehiclesAPI = {
	getVehicles (currentPage) {
		return instance.get(`vehicles/?page=${currentPage}`).then(response => {
			return response.data});
	},
	getInfo (id) {
		return instance.get(`vehicles/${id}/`).then(response => {
			return response.data});
	}
}