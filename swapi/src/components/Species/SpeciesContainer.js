import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Species from './Species';
import {requestSpecies} from '../../redux/species-reducer';
import Preloader from '../Common/Preloader/Preloader';
import ErrorApi from '../Common/ErrorApi/ErrorApi';

class SpeciesContainer extends React.Component {
	componentDidMount() {
		const {currentPage} = this.props;
		this.props.requestSpecies(currentPage);
	}
	onPageChenged = (currentPage) => {
		this.props.requestSpecies(currentPage);
	}
	
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<Species 
								species={this.props.species}
								onPageChenged={this.onPageChenged}
								totalEssenceCount={this.props.totalEssenceCount}
								pageSize={this.props.pageSize}
								currentPage={this.props.currentPage}
							/>
						</>
					)
				}
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		species: state.speciesPage.species,
		currentPage: state.speciesPage.currentPage,
		totalEssenceCount: state.speciesPage.totalEssenceCount,
		pageSize: state.speciesPage.pageSize,
		isFetching: state.speciesPage.isFetching,
		isErrorApi: state.speciesPage.isErrorApi,
		errorMessage: state.speciesPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestSpecies})) (SpeciesContainer);