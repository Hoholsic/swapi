import React from 'react';
import style from './Species.module.css';
import {NavLink} from 'react-router-dom';
import Paginator from '../Common/Paginator/Paginator'

const Species = ({species, totalEssenceCount, pageSize, currentPage, onPageChenged}) => {
	return (
		<div className={style.species}>
			<Paginator 
				totalEssenceCount={totalEssenceCount}
				pageSize={pageSize}
				currentPage={currentPage}
				onPageChenged={onPageChenged} 
				path={'/species/?page='}
			/>	
			<div className={style.species__list}>
				{species.map(({id, name, img}) => <div key={id}  className={style.species__item}>
					<NavLink to={`/species/${id}`}>
						<img 
							src={img} 
							alt={name} 
							className={style.species__photo}
						/>
						<div className={style.species__name}>{name}</div>
					</NavLink>
				</div>)}
			</div>
		</div>
	)
}

export default Species;