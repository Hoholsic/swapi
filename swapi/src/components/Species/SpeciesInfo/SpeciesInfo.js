import React from 'react';
import style from './SpeciesInfo.module.css';
import Link from '../../Common/Link/Link';
import MoreInfoImg from '../../Common/MoreInfoImg/MoreInfoImg';
import Info from '../../Common/Info/Info';

const SpeciesInfo = ({itemInfo, speciesName, speciesPhoto, handleGoBack, films, people, homeworld}) => {
	return (
		<div className={style.species}>
			<Link handleGoBack={handleGoBack}/>
			<h3 className={style.species__name}>{speciesName}</h3>
			<div className={style.species__cart}>
				<div className={style.species__profile} >
					<img 
						src={speciesPhoto} 
						alt={speciesName} 
						className={style.species__photo}
					/>
				</div>
				<Info itemInfo={itemInfo}/>
			</div>	
			<MoreInfoImg 
				information={people} 
				path={'/people/'} 
				text={'сharacters'}
			/>
			<MoreInfoImg 
				information={films} 
				path={'/films/'} 
				text={'films'}
			/>
		</div>
	)
}

export default SpeciesInfo;