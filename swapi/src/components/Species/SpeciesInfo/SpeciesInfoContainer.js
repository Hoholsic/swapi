import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import {requestSpeciesInfo} from '../../../redux/species-reducer';
import SpeciesInfo from './SpeciesInfo';
import Preloader from '../../Common/Preloader/Preloader';
import ErrorApi from '../../Common/ErrorApi/ErrorApi';

class SpeciesInfoContainer extends React.Component {
	componentDidMount() {
		let id = this.props.match.params.id;
		this.props.requestSpeciesInfo(id);			
	}
	handleGoBack = (event) => {
		event.preventDefault();
		this.props.history.goBack();
	}
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<SpeciesInfo 
								itemInfo={this.props.itemInfo}
								speciesName={this.props.speciesName}
								speciesPhoto={this.props.speciesPhoto}
								handleGoBack={this.handleGoBack}
								films={this.props.films}
								people={this.props.people}
							/>
						</>
					)
				}
			</div>
		);
	}
}
let mapStateToProps = (state) => ({
	itemInfo: state.speciesPage.itemInfo,
	speciesName: state.speciesPage.speciesName,
	speciesPhoto: state.speciesPage.speciesPhoto,
	films: state.speciesPage.films,
	people: state.speciesPage.people,
	isFetching: state.speciesPage.isFetching,
	isErrorApi: state.speciesPage.isErrorApi,
	errorMessage: state.speciesPage.errorMessage,
});

export default compose(connect(mapStateToProps, {requestSpeciesInfo}), withRouter)(SpeciesInfoContainer);