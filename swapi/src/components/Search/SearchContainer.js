import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import {searchCharacter, setValueInput} from '../../redux/search-reducer';
import Search from './Search';

class SearchContainer extends React.Component {
	componentDidMount() {		
	}

	handleInputChange = (event) => {
		const valueInput = event.target.value;
		this.props.searchCharacter(valueInput)
	}

	render() {
		return (
			<div>
				<Search 
					handleInputChange={this.handleInputChange}
					valueInput={this.props.valueInput}
					characters={this.props.characters}
				/>
			</div>
		);
	}
}
let mapStateToProps = (state) => {
	return {
		characters: state.searchPage.characters,
		valueInput: state.searchPage.valueInput,
		isFetching: state.searchPage.isFetching,
	}
}

export default compose(connect(mapStateToProps, {searchCharacter, setValueInput}), withRouter)(SearchContainer);