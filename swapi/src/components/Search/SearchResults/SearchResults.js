import React from 'react';
import {NavLink} from 'react-router-dom';
import style from './SearchResults.module.css';

const SearchResults = ({characters}) => {
	return (
		<div className={style.characters}>
			{characters.length
				? (
						<div className={style.characters__list}>
							{characters.map(({id, name, img}) => <div key={id} className={style.characters__item} >
								<NavLink to={`/people/${id}`}>
									<img src={img} alt={name} className={style.characters__photo}/>
									<div className={style.characters__name}>{name}</div>
								</NavLink>
							</div>)}
						</div>
					)
				: <h2 className={style.coment} >No results</h2>		
			}
		</div>
	)
}

export default SearchResults;

