import React from 'react';
import style from './Search.module.css';
import SearchResults from './SearchResults/SearchResults';

const Search = ({valueInput, characters, handleInputChange}) => {
	return (
		<>
		<div className={style.search}>
			<input 
				type="text"
				value={valueInput}
				onChange={handleInputChange}
				className={style.search__input}
				placeholder="enter character name"
			/>
		</div>
			<SearchResults characters={characters}/>
		</>
	)
}

export default Search;