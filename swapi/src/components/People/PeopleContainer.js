import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import People from './People';
import {requestPeople} from '../../redux/people-reducer';
import Preloader from '../Common/Preloader/Preloader';
import ErrorApi from '../Common/ErrorApi/ErrorApi';

class PeopleContainer extends React.Component {
	componentDidMount() {
		const {currentPage} = this.props;
		this.props.requestPeople(currentPage);
	}
	onPageChenged = (currentPage) => {
		this.props.requestPeople(currentPage);
	}
	
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
						{this.props.isFetching ? <Preloader /> : null}
						<People 
							people={this.props.people}
							onPageChenged={this.onPageChenged}
							totalEssenceCount={this.props.totalEssenceCount}
							pageSize={this.props.pageSize}
							currentPage={this.props.currentPage}
						/>
						</>
					)
				}				
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		people: state.peoplePage.people,
		currentPage: state.peoplePage.currentPage,
		totalEssenceCount: state.peoplePage.totalEssenceCount,
		pageSize: state.peoplePage.pageSize,
		isFetching: state.peoplePage.isFetching,
		isErrorApi: state.peoplePage.isErrorApi,
		errorMessage: state.peoplePage.errorMessage
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestPeople})) (PeopleContainer);