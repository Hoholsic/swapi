import React from 'react';
import style from './PeopleInfo.module.css';
import {NavLink} from 'react-router-dom';
import Link from '../../Common/Link/Link';
import Homeworld from '../../Common/Homeworld/Homeworld';
import Info from '../../Common/Info/Info';
import MoreInfoImg from '../../Common/MoreInfoImg/MoreInfoImg';
import MoreInfoName from '../../Common/MoreInfoName/MoreInfoName';

const PeopleInfo = ({itemInfo, personName, personPhoto, handleGoBack,
				 films, personSpecies, homeworld, starships, vehicles}) => {
	return (
		<div className={style.person}>
			<Link handleGoBack={handleGoBack}/>
			<h3 className={style.person__name}>{personName}</h3>
			<div className={style.person__cart}>
				<div className={style.person__profile} >
					<img 
						src={personPhoto} 
						alt={personName} 
						className={style.person__photo}
					/>
				</div>
				<div className={style.person__info}>
					<Homeworld homeworld={homeworld}/>
					{personSpecies.map(({id, name}) => <NavLink to={`/species/${id}`} key={id} className={style.person__homeworld}>
						<span className={style.person__title}>Species:</span> <span className={style.person__value}>{name}</span>
					</NavLink>)}
					<Info itemInfo={itemInfo}/>
				</div>
			</div>	
			<MoreInfoImg 
				information={films} 
				path={'/films/'} 
				text={'films'}
			/>
			<div className={style.person__block}>
				<MoreInfoName 
					information={starships} 
					path={'/starships/'} 
					text={'starships'}
				/>
				<MoreInfoName 
					information={vehicles} 
					path={'/vehicles/'} 
					text={'vehicles'}
				/>
			</div>
		</div>
	)
}

export default PeopleInfo;