import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import {requestPerson} from '../../../redux/people-reducer.js';
import PeopleInfo from './PeopleInfo';
import Preloader from '../../Common/Preloader/Preloader';
import ErrorApi from '../../Common/ErrorApi/ErrorApi';

class PeopleInfoContainer extends React.Component {
	componentDidMount() {
		let id = this.props.match.params.id;
		this.props.requestPerson(id);			
	}
	handleGoBack = (event) => {
		event.preventDefault();
		this.props.history.goBack();
	}
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
						{this.props.isFetching ? <Preloader /> : null}
						<PeopleInfo 
							itemInfo={this.props.itemInfo}
							personName={this.props.personName}
							personPhoto={this.props.personPhoto}
							handleGoBack={this.handleGoBack}
							films={this.props.films}
							personSpecies={this.props.personSpecies}
							homeworld={this.props.homeworld}
							starships={this.props.starships}
							vehicles={this.props.vehicles}
						/>
						</>
					)
				}
			</div>
		);
	}
}
let mapStateToProps = (state) => ({
	itemInfo: state.peoplePage.itemInfo,
	personName: state.peoplePage.personName,
	personPhoto: state.peoplePage.personPhoto,
	films: state.peoplePage.films,
	personSpecies: state.peoplePage.personSpecies,
	homeworld: state.peoplePage.homeworld,
	starships: state.peoplePage.starships,
	vehicles: state.peoplePage.vehicles,
	isFetching: state.peoplePage.isFetching,
	isErrorApi: state.peoplePage.isErrorApi,
	errorMessage: state.peoplePage.errorMessage,
});

export default compose(connect(mapStateToProps, {requestPerson}), withRouter)(PeopleInfoContainer);