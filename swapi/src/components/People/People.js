import React from 'react';
import style from './People.module.css';
import {NavLink} from 'react-router-dom';
import Paginator from '../Common/Paginator/Paginator';

const People = ({people, totalEssenceCount, pageSize, currentPage, onPageChenged}) => {
	return (
		<div className={style.people}>
			<Paginator 
				totalEssenceCount={totalEssenceCount}
				pageSize={pageSize}
				currentPage={currentPage}
				onPageChenged={onPageChenged} 
				path={'/people/?page='}
			/>	
			<div className={style.people__list}>
				{people.map(({id, name, img}) => <div key={id}  className={style.people__item}>
					<NavLink to={`/people/${id}`}>
						<img 
							src={img} 
							alt={name} 
							className={style.people__photo}
						/>
						<div className={style.people__name}>{name}</div>
					</NavLink>
				</div>)}
			</div>
		</div>
	)
}

export default People;