import React from 'react';
import style from './ErrorApi.module.css';
import error from '../../../image/error.png';
import {NavLink} from 'react-router-dom';

const ErrorApi = ({errorMessage}) => {
	return(
		<div className={style.error}>
			<p className={style.error__text}>{errorMessage}</p>
			<img 
				src={error} 
				alt="error"
				className={style.error__img}
			/>
			<div>
				<NavLink to={"/home"} className={style.error__btn}><u>Go to home</u></NavLink>
			</div>
		</div>
	)
}

export default ErrorApi;