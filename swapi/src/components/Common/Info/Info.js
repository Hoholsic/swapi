import React from 'react';
import style from './Info.module.css';

const Info = ({itemInfo}) => {
	return (
		<div className={style.info}>
			{itemInfo.map(({title, value}) => <div key={title}>
				<span className={style.info__subtitle}>{title}:</span> <span className={style.info__value}>{value}</span>
			</div>)}
		</div>
	)
}

export default Info;