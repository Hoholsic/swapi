import React from 'react';
import style from './FirstScreen.module.css';
import firstScreen from '../../../image/firstScreen.jpg';

const FirstScreen = (props) => {
	return(
		<img src={firstScreen} alt="firstScreen" className={style.fullscreen} />
	)
}

export default FirstScreen;