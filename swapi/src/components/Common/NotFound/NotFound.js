import React from 'react';
import {useLocation} from 'react-router-dom';
import style from './NotFound.module.css';
import notFound from '../../../image/notFound.png';

const NotFound = (props) => {
	let locotion = useLocation();
	return (
		<div className={style.notFound}>
			<p className={style.notFound__code}>404</p>
			<p className={style.notFound__text}>you lost your way my son</p>
			<img 
				src={notFound} 
				alt="404" 
				className={style.notFound__img}
			/>
			<p className={style.notFound__path}>No match for: <u>{locotion.pathname}</u></p>
		</div>
	)
}

export default NotFound;