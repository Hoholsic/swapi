import React from 'react';
import style from './Link.module.css';
import goBack from '../../../image/goBack.png';

const Link = ({handleGoBack}) => {
	return(
		<a href="#"
			className={style.link}
			onClick={handleGoBack}
		>	
			<img src={goBack} alt="goBack" className={style.link__img} />
			Go Back
		</a>
	)
}

export default Link;