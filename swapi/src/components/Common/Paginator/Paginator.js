import React, {useState} from 'react';
import style from './Paginator.module.css';
import cn from 'classnames';
import {Link} from 'react-router-dom';

const Paginator = ({totalEssenceCount, pageSize, currentPage, onPageChenged, portionSize = 3, path}) => {
	let pagesCount = Math.ceil(totalEssenceCount / pageSize);
	let pages = [];
	for (let i = 1; i <= pagesCount; i++) {pages.push(i)}

	let portionCount = Math.ceil(pagesCount / portionSize);
	let [portionNumber, setPortionNumber] = useState(1)
	let leftPortionPegaNumber = (portionNumber - 1) * portionSize + 1;
	let rightPortionPegaNumber = portionNumber * portionSize;

	return(
		<div className={style.pagination}>
			<button 
				className={style.pagination__btn} 
				onClick={() => {setPortionNumber(portionNumber - 1)}}
				disabled={portionNumber <= 1}
			> 
				&#60; 
			</button>
			{pages
				.filter(p => p >= leftPortionPegaNumber && p <= rightPortionPegaNumber)
				.map((p) => {
				return <Link to={`${path}${p}`} key={p} 
						className={cn({[style.pagination__page]: currentPage === p}, style.pagination__pageNumber)}
						onClick={(e) => {onPageChenged(p);}}>{p}
					</Link>})
			}
			<button 
				className={style.pagination__btn} 
				onClick={() => {setPortionNumber(portionNumber + 1)}}
				disabled={portionNumber >= portionCount}
			>
				&#62;
			</button>
		</div>
	)
}

export default Paginator;