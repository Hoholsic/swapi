import React from 'react';
import style from './MoreInfoName.module.css';
import {NavLink} from 'react-router-dom';

const MoreInfoName = ({information, path, text}) => {
	return (
		<div className={style.information}>
			<h4 className={style.information__title}>{text}</h4>
				{information.length
					? (
						<div className={style.information__items}>
							{information.map(({id, name}) => <div key={id} className={style.information__item}>
								<NavLink to={`${path}${id}`}>
									<p className={style.information__name}>{name}</p>
								</NavLink>
							</div>)}
						</div>
					)
					: <h2 className={style.information__coment}>No results</h2>
				}
		</div>
	)
}

export default MoreInfoName;