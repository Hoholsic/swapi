import React from 'react';
import style from './Homeworld.module.css';
import {NavLink} from 'react-router-dom';

const Homeworld = ({homeworld}) => {
	return (
		<div>
			{homeworld.length
				?(
					<div className={style.homeworld}>
						{homeworld.map(({id, title, value}) => <div key={id}>
							<NavLink to={`/planets/${id}`}  className={style.homeworld__item}>
								<span className={style.homeworld__title}>{title}:</span> 
								<span className={style.homeworld__value}>{value}</span>
							</NavLink>		
						</div>)}
					</div>
				)
				: " "
			}
		</div>		
	)
}

export default Homeworld;