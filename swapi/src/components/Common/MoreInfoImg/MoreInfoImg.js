import React from 'react';
import style from './MoreInfoImg.module.css';
import {NavLink} from 'react-router-dom';

const MoreInfoImg = ({information, path, text}) => {
	return (
		<div className={style.information}>
			<h4 className={style.information__title}>{text}</h4>
				{information.length
					? (
						<div className={style.information__items}>
							{information.map(({id, img}) => <div key={id} className={style.information__item}>
								<NavLink to={`${path}${id}`}>
									<img src={img} alt={id} className={style.information__photo}/>
								</NavLink>
							</div>)}
						</div>
					)
					: <h2 className={style.information__coment}>No results</h2>
				}
		</div>
	)
}

export default MoreInfoImg;