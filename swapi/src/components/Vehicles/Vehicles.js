import React from 'react';
import style from './Vehicles.module.css';
import {NavLink} from 'react-router-dom';
import Paginator from '../Common/Paginator/Paginator';

const Vehicles = ({vehicles, onPageChenged, totalEssenceCount, pageSize, currentPage}) => {
	return (
		<div className={style.vehicles}>
			<Paginator 
				totalEssenceCount={totalEssenceCount}
				pageSize={pageSize}
				currentPage={currentPage}
				onPageChenged={onPageChenged} 
				path={'/vehicles/?page='}
			/>	
			<div className={style.vehicles__list}>
				{vehicles.map(({id, name, img}) => <div key={id}  className={style.vehicles__item}>
					<NavLink to={`/vehicles/${id}`}>
						<div style={{backgroundImage: `url(${img})`}} className={style.vehicles__photo}></div>
						<div className={style.vehicles__name}>{name}</div>
					</NavLink>
				</div>)}
			</div>
		</div>
	)
}

export default Vehicles;