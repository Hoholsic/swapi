import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Vehicles from './Vehicles';
import {requestVehicles} from '../../redux/vehicles-reducer';
import Preloader from '../Common/Preloader/Preloader';
import ErrorApi from '../Common/ErrorApi/ErrorApi';

class VehiclesContainer extends React.Component {
	componentDidMount() {
			const {currentPage} = this.props;
			this.props.requestVehicles(currentPage);
	}
	onPageChenged = (currentPage) => {
		this.props.requestVehicles(currentPage);
	}
	
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<Vehicles 
								vehicles={this.props.vehicles}
								onPageChenged={this.onPageChenged}
								totalEssenceCount={this.props.totalEssenceCount}
								pageSize={this.props.pageSize}
								currentPage={this.props.currentPage}
							/>
						</>
					)
				}
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		vehicles: state.vehiclesPage.vehicles,
		currentPage: state.vehiclesPage.currentPage,
		totalEssenceCount: state.vehiclesPage.totalEssenceCount,
		pageSize: state.vehiclesPage.pageSize,
		isFetching: state.vehiclesPage.isFetching,
		isErrorApi: state.vehiclesPage.isErrorApi,
		errorMessage: state.vehiclesPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestVehicles})) (VehiclesContainer);