import React from 'react';
import style from './VehicleInfo.module.css';
import Link from '../../Common/Link/Link';
import MoreInfoImg from '../../Common/MoreInfoImg/MoreInfoImg';
import Info from '../../Common/Info/Info';

const VehicleInfo = ({itemInfo, vehiclesName, vehiclesPhoto, handleGoBack, people, films}) => {
	return (
		<div className={style.vehicle}>
			<Link handleGoBack={handleGoBack}/>
			<h3 className={style.vehicle__title}>{vehiclesName}</h3>
			<div className={style.vehicle__cart}>
				<div className={style.vehicle__profile} >
					<div style={{backgroundImage: `url(${vehiclesPhoto})`}} className={style.vehicle__photo}></div>
				</div>
				<Info itemInfo={itemInfo}/>
			</div>
			<MoreInfoImg 
				information={people} 
				path={'/people/'} 
				text={'сharacters'}
			/>
			<MoreInfoImg 
				information={films} 
				path={'/films/'} 
				text={'films'}
			/>
		</div>
	)
}

export default VehicleInfo;