import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import VehicleInfo from './VehicleInfo';
import {requestVehiclesInfo} from '../../../redux/vehicles-reducer';
import Preloader from '../../Common/Preloader/Preloader';
import ErrorApi from '../../Common/ErrorApi/ErrorApi';

class VehicleInfoContainer extends React.Component {
	componentDidMount() {
		let id = this.props.match.params.id;
		this.props.requestVehiclesInfo(id);	
	}
	handleGoBack = (event) => {
		event.preventDefault();
		this.props.history.goBack();
	}
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<VehicleInfo 
								itemInfo={this.props.itemInfo}
								vehiclesName={this.props.vehiclesName}
								vehiclesPhoto={this.props.vehiclesPhoto}
								handleGoBack={this.handleGoBack}
								people={this.props.people}
								films={this.props.films}
							/>
						</>
					)
				}
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		itemInfo: state.vehiclesPage.itemInfo,
		vehiclesName: state.vehiclesPage.vehiclesName,
		vehiclesPhoto: state.vehiclesPage.vehiclesPhoto,
		isFetching: state.vehiclesPage.isFetching,
		people: state.vehiclesPage.people,
		films: state.vehiclesPage.films,
		isErrorApi: state.vehiclesPage.isErrorApi,
		errorMessage: state.vehiclesPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestVehiclesInfo})) (VehicleInfoContainer);