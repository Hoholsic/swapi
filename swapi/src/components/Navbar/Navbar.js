import React from 'react';
import {NavLink} from 'react-router-dom';
import style from './Navbar.module.css';
import logo from '../../image/logo.png';
import BurgerMenu from './BurgerMenu/BurgerMenu';
import BurgerIcon from './BurgerIcon/BurgerIcon';

const Navbar = ({menu, changeBurger, activeBurger, closeBurgerMenu}) => {
	return (
		<nav className = {style.navbar}>
			<div className={style.navbar__logo}>
				<img src={logo} alt="stara wars"/>
			</div>
			<div className={style.navbar__items}>
				{menu.map(({id, value, path}) => <div key={id}>
					<NavLink className={style.item} activeClassName={style.active} to={path}>{value}</NavLink>
				</div>)}
			</div>
			<BurgerIcon 
				changeBurger={changeBurger}
				activeBurger={activeBurger}
			/>
			<BurgerMenu 
				menu={menu}
				activeBurger={activeBurger}
				closeBurgerMenu={closeBurgerMenu}
			/>
	 	</nav>
 	);
}

export default Navbar;