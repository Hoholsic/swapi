import React from 'react';
import './BurgerIcon.css';

const BurgerIcon = ({changeBurger, activeBurger}) => {
	return (
		<div 
			className={activeBurger ? "burger active" : "burger"}
			onClick={changeBurger}
		>
			<span></span>
		</div>
 	);
}

export default BurgerIcon;