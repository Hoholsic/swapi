import React, {useState} from 'react';
import Navbar from './Navbar';

const NavbarContainer = (props) => {
	let [activeBurger, setActiveBurger] = useState(false);
	const menu = [
		{id: '1', value: 'home', path: '/home'},
		{id: '2', value: 'people', path: '/people/?page=1'},
		{id: '3', value: 'films', path: '/films/?page=1'},
		{id: '4', value: 'species', path: '/species/?page=1'},
		{id: '5', value: 'planets', path: '/planets/?page=1'},
		{id: '6', value: 'starships', path: '/starships/?page=1'},
		{id: '7', value: 'vehicles', path: '/vehicles/?page=1'},
		{id: '8', value: 'search', path: '/search'},
	]

	let changeBurger = () => {
		setActiveBurger(!activeBurger);
	}
	let closeBurgerMenu = () => {
		setActiveBurger(false);
	}
	return (
		<>
			<Navbar 	
				menu={menu}
				changeBurger={changeBurger}
				activeBurger={activeBurger}
				closeBurgerMenu={closeBurgerMenu}
			/>
		</>
 	);
}

export default NavbarContainer;