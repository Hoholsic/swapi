import React from 'react';
import {NavLink} from 'react-router-dom';
import './BurgerMenu.css';

const BurgerMenu = ({menu, activeBurger, closeBurgerMenu}) => {
	return (
		<div 
			className={activeBurger ? "menu active" : "menu"}
			onClick={closeBurgerMenu}
		>
			<div className="menu__blur"/>
			<div className="menu__body">
				{menu.map(({id, value, path}) => <div key={id} className="menu__item">
					<NavLink to={path} className="menu__link" activeClassName="menu__activeLink">
						{value}
					</NavLink>
				</div>)}	
			</div>
		</div>
 	);
}

export default BurgerMenu;