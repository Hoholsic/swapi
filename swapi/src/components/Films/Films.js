import React from 'react';
import {NavLink} from 'react-router-dom';
import style from './Films.module.css';


const Films = ({films}) => {
	return (
		<div className={style.films}>
			{films
				.sort((a, b) => a.episode_id - b.episode_id)
				.map(({title, id, episode_id, img}) => <div key={id} className={style.films__item}>
				<NavLink to={`/films/${id}`}>
					<img 
						src={img} 
						alt={title} 
						className={style.films__poster} 
					/>
					<div className={style.films__name} >
						<p>Episode: {episode_id}</p>
						<p>{title}</p>
					</div>
				</NavLink>
			</div>)}
		</div>
	)
}

export default Films;