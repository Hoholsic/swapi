import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import FilmInfo from './FilmInfo';
import {requestFilmInfo} from '../../../redux/films-reducer';
import Preloader from '../../Common/Preloader/Preloader';
import ErrorApi from '../../Common/ErrorApi/ErrorApi';

class FilmInfoContainer extends React.Component {
	componentDidMount() {
		let id = this.props.match.params.id;
		this.props.requestFilmInfo(id);	
	}
	handleGoBack = (event) => {
		event.preventDefault();
		this.props.history.goBack();
	}
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<FilmInfo 
								itemInfo={this.props.itemInfo}
								filmTitle={this.props.filmTitle}
								filmPoster={this.props.filmPoster}
								handleGoBack={this.handleGoBack}
								people={this.props.people}
								species={this.props.species}
								planets={this.props.planets}
								starships={this.props.starships}
								vehicles={this.props.vehicles}
							/>
						</>
					)
				}	
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		itemInfo: state.filmsPage.itemInfo,
		filmTitle: state.filmsPage.filmTitle,
		filmPoster: state.filmsPage.filmPoster,
		isFetching: state.filmsPage.isFetching,
		people: state.filmsPage.people,
		planets: state.filmsPage.planets,
		starships: state.filmsPage.starships,
		vehicles: state.filmsPage.vehicles,
		species: state.filmsPage.species,
		isErrorApi: state.filmsPage.isErrorApi,
		errorMessage: state.filmsPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestFilmInfo})) (FilmInfoContainer);