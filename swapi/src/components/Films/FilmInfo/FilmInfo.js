import React from 'react';
import style from './FilmInfo.module.css';
import Link from '../../Common/Link/Link';
import Info from '../../Common/Info/Info';
import MoreInfoImg from '../../Common/MoreInfoImg/MoreInfoImg';
import MoreInfoName from '../../Common/MoreInfoName/MoreInfoName';

const FilmInfo = ({itemInfo, filmTitle, filmPoster, handleGoBack,
				 people, species, planets, starships, vehicles}) => {
	return (
		<div className={style.film}>
			<Link handleGoBack={handleGoBack}/>
			<h3 className={style.film__title}>{filmTitle}</h3>
			<div className={style.film__cart}>
				<div className={style.film__profile} >
					<img 
						src={filmPoster} 
						alt={filmTitle} 
						className={style.film__poster}
					/>
				</div>
				<Info itemInfo={itemInfo}/>
			</div>
			<MoreInfoImg 
				information={people} 
				path={'/people/'} 
				text={'сharacters'}
			/>
			<MoreInfoImg 
				information={species} 
				path={'/species/'} 
				text={'species'}
			/>
			<div className={style.film__items}>
				<MoreInfoName 
					information={planets} 
					path={'/planets/'} 
					text={'planets'}
				/>
				<MoreInfoName 
					information={starships} 
					path={'/starships/'} 
					text={'starships'}
				/>
				<MoreInfoName 
					information={vehicles} 
					path={'/vehicles/'} 
					text={'vehicles'}
				/>
			</div>
		</div>
	)
}

export default FilmInfo;