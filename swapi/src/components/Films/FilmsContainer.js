import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Films from './Films';
import {requestFilms} from '../../redux/films-reducer';
import Preloader from '../Common/Preloader/Preloader';
import ErrorApi from '../Common/ErrorApi/ErrorApi';

class FilmsContainer extends React.Component {
	componentDidMount() {
		const {currentPage} = this.props;
		this.props.requestFilms(currentPage);
	}
	
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<Films films={this.props.films}/>
						</>
					)
				}	
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		films: state.filmsPage.films,
		currentPage: state.filmsPage.currentPage,
		isFetching: state.filmsPage.isFetching,
		isErrorApi: state.filmsPage.isErrorApi,
		errorMessage: state.filmsPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestFilms})) (FilmsContainer);