import React from 'react';
import style from './Planets.module.css';
import {NavLink} from 'react-router-dom';
import Paginator from '../Common/Paginator/Paginator';

const Planets = ({planets, totalEssenceCount, pageSize, currentPage, onPageChenged}) => {
	return (
			<div className={style.planets}>
				<Paginator 
					totalEssenceCount={totalEssenceCount}
					pageSize={pageSize}
					currentPage={currentPage}
					onPageChenged={onPageChenged} 
					path={'/planets/?page='}
				/>	
				<div className={style.planets__list}>
					{planets.map(({id, name, img}) => <div key={id}  className={style.planets__item}>
						<NavLink to={`/planets/${id}`}>
							<div style={{backgroundImage: `url(${img})`}} className={style.planets__photo}></div>
							<div className={style.planets__name}>{name}</div>
						</NavLink>
					</div>)}
				</div>
			</div>
	)
}

export default Planets;