import React from 'react';
import style from './PlanetInfo.module.css';
import Link from '../../Common/Link/Link';
import Info from '../../Common/Info/Info';
import MoreInfoImg from '../../Common/MoreInfoImg/MoreInfoImg';

const PlanetInfo = ({itemInfo, plenetName, plenetPhoto, handleGoBack, people, films}) => {
	return (
		<div className={style.planet}>
			<Link handleGoBack={handleGoBack}/>
			<h3 className={style.planet__title}>{plenetName}</h3>
			<div className={style.planet__cart}>
				<div className={style.planet__profile} >
					<div style={{backgroundImage: `url(${plenetPhoto})`}} className={style.planet__photo}></div>
				</div>
				<Info itemInfo={itemInfo}/>
			</div>
			<MoreInfoImg 
				information={people} 
				path={'/people/'} 
				text={'сharacters'}
			/>
			<MoreInfoImg 
				information={films} 
				path={'/films/'} 
				text={'films'}
			/>
		</div>
	)
}

export default PlanetInfo;