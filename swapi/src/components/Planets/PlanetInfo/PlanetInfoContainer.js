import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import PlanetInfo from './PlanetInfo';
import {requestPlanetInfo} from '../../../redux/planets-reducer';
import Preloader from '../../Common/Preloader/Preloader';
import ErrorApi from '../../Common/ErrorApi/ErrorApi';

class PlanetInfoContainer extends React.Component {
	componentDidMount() {
		let id = this.props.match.params.id;
		this.props.requestPlanetInfo(id);	
	}
	handleGoBack = (event) => {
		event.preventDefault();
		this.props.history.goBack();
	}
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<PlanetInfo 
								itemInfo={this.props.itemInfo}
								plenetName={this.props.plenetName}
								plenetPhoto={this.props.plenetPhoto}
								handleGoBack={this.handleGoBack}
								people={this.props.people}
								films={this.props.films}
							/>
						</>
					)
				}
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		itemInfo: state.planetsPage.itemInfo,
		plenetName: state.planetsPage.plenetName,
		plenetPhoto: state.planetsPage.plenetPhoto,
		isFetching: state.planetsPage.isFetching,
		people: state.planetsPage.people,
		films: state.planetsPage.films,
		isErrorApi: state.planetsPage.isErrorApi,
		errorMessage: state.planetsPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestPlanetInfo})) (PlanetInfoContainer);