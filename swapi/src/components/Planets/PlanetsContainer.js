import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Planets from './Planets';
import {requestPlanets} from '../../redux/planets-reducer';
import Preloader from '../Common/Preloader/Preloader';
import ErrorApi from '../Common/ErrorApi/ErrorApi';

class PeopleContainer extends React.Component {
	componentDidMount() {
			const {currentPage} = this.props;
			this.props.requestPlanets(currentPage);
	}
	onPageChenged = (currentPage) => {
		this.props.requestPlanets(currentPage);
	}
	
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<Planets 
								planets={this.props.planets}
								onPageChenged={this.onPageChenged}
								totalEssenceCount={this.props.totalEssenceCount}
								pageSize={this.props.pageSize}
								currentPage={this.props.currentPage}
							/>
						</>
					)
				}	
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		planets: state.planetsPage.planets,
		currentPage: state.planetsPage.currentPage,
		totalEssenceCount: state.planetsPage.totalEssenceCount,
		pageSize: state.planetsPage.pageSize,
		isFetching: state.planetsPage.isFetching,
		isErrorApi: state.planetsPage.isErrorApi,
		errorMessage: state.planetsPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestPlanets})) (PeopleContainer);