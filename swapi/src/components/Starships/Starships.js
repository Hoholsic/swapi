import React from 'react';
import style from './Starships.module.css';
import {NavLink} from 'react-router-dom';
import Paginator from '../Common/Paginator/Paginator';

const Starships = ({starships, onPageChenged, totalEssenceCount, pageSize, currentPage}) => {
	return (
		<div className={style.starships}>
			<Paginator 
				totalEssenceCount={totalEssenceCount}
				pageSize={pageSize}
				currentPage={currentPage}
				onPageChenged={onPageChenged} 
				path={'/starships/?page='}
			/>	
			<div className={style.starships__list}>
				{starships.map(({id, name, img}) => <div key={id}  className={style.starships__item}>
					<NavLink to={`/starships/${id}`}>
						<div style={{backgroundImage: `url(${img})`}} className={style.starships__photo}></div>
						<div className={style.starships__name}>{name}</div>
					</NavLink>
				</div>)}
			</div>
		</div>
	)
}

export default Starships;