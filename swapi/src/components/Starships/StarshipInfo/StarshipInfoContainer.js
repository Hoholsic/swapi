import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import StarshipInfo from './StarshipInfo';
import {requestStarshipsInfo} from '../../../redux/starships-reducer';
import Preloader from '../../Common/Preloader/Preloader';
import ErrorApi from '../../Common/ErrorApi/ErrorApi';

class StarshipInfoContainer extends React.Component {
	componentDidMount() {
		let id = this.props.match.params.id;
		this.props.requestStarshipsInfo(id);	
	}
	handleGoBack = (event) => {
		event.preventDefault();
		this.props.history.goBack();
	}
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<StarshipInfo 
								itemInfo={this.props.itemInfo}
								starshipsName={this.props.starshipsName}
								starshipsPhoto={this.props.starshipsPhoto}
								handleGoBack={this.handleGoBack}
								people={this.props.people}
								films={this.props.films}
							/>
						</>
					)
				}
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		itemInfo: state.starshipsPage.itemInfo,
		starshipsName: state.starshipsPage.starshipsName,
		starshipsPhoto: state.starshipsPage.starshipsPhoto,
		isFetching: state.starshipsPage.isFetching,
		people: state.starshipsPage.people,
		films: state.starshipsPage.films,
		isErrorApi: state.starshipsPage.isErrorApi,
		errorMessage: state.starshipsPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestStarshipsInfo})) (StarshipInfoContainer);