import React from 'react';
import style from './StarshipInfo.module.css';
import Link from '../../Common/Link/Link';
import MoreInfoImg from '../../Common/MoreInfoImg/MoreInfoImg';
import Info from '../../Common/Info/Info';

const StarshipInfo = ({itemInfo, starshipsName, starshipsPhoto, handleGoBack, people, films}) => {
	return (
		<div className={style.starships}>
			<Link handleGoBack={handleGoBack}/>
			<h3 className={style.starships__title}>{starshipsName}</h3>
			<div className={style.starships__cart}>
				<div className={style.starships__profile} >
					<div style={{backgroundImage: `url(${starshipsPhoto})`}} className={style.starships__photo}></div>
				</div>
				<Info itemInfo={itemInfo}/>
			</div>
			<MoreInfoImg 
				information={people}
				path={'/people/'} 
				text={'сharacters'}
			/>
			<MoreInfoImg 
				information={films} 
				path={'/films/'} 
				text={'films'}
			/>
		</div>
	)
}

export default StarshipInfo;