import React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Starships from './Starships';
import {requestStarships} from '../../redux/starships-reducer';
import Preloader from '../Common/Preloader/Preloader';
import ErrorApi from '../Common/ErrorApi/ErrorApi';

class StarshipsContainer extends React.Component {
	componentDidMount() {
			const {currentPage} = this.props;
			this.props.requestStarships(currentPage);
	}
	onPageChenged = (currentPage) => {
		this.props.requestStarships(currentPage);
	}
	
	render() {
		return (
			<div>
				{this.props.isErrorApi
					? <ErrorApi errorMessage={this.props.errorMessage}/>
					: (<>
							{this.props.isFetching ? <Preloader /> : null}
							<Starships 
								starships={this.props.starships}
								onPageChenged={this.onPageChenged}
								totalEssenceCount={this.props.totalEssenceCount}
								pageSize={this.props.pageSize}
								currentPage={this.props.currentPage}
							/>
						</>
					)
				}
			</div>
		);
	}
}

let mapStateToProps = (state) => {
	return {
		starships: state.starshipsPage.starships,
		currentPage: state.starshipsPage.currentPage,
		totalEssenceCount: state.starshipsPage.totalEssenceCount,
		pageSize: state.starshipsPage.pageSize,
		isFetching: state.starshipsPage.isFetching,
		isErrorApi: state.starshipsPage.isErrorApi,
		errorMessage: state.starshipsPage.errorMessage,
	}
}

export default compose(withRouter, connect(mapStateToProps, {requestStarships})) (StarshipsContainer);